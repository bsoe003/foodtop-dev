from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from foods.forms import UserForm, UserProfileForm, PostForm, CommentForm, ReportForm
from foods.models import UserProfile, Post, Comment, Category, Report
import json

def home(request):
    """
    View for homepage. Shows current top food if loggedin.
    If not logged in, it will direct to login page.
    """
    context = RequestContext(request)
    content = {'user': request.user, 'message': ''}
    status = str(request.GET.get('logout',''))
    action = str(request.GET.get('action',''))
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    content['profile'] = UserProfile.objects.get(user=request.user)
    if request.method == 'POST':
        for c in content['profile'].subscribed.all():
            content['profile'].subscribed.remove(c)
        for c in request.POST:
            if c == 'csrfmiddlewaretoken':
                continue
            category_toadd = Category.objects.get(id=int(c))
            print category_toadd
            content['profile'].subscribed.add(category_toadd)
        return HttpResponseRedirect('/')
    if status == user: # logout if query
        return user_logout(request)
    if action == 'removed':
        content['message'] = 'Removed Successfully'
    content['all'] = Post.objects.all()
    #content['overall'] = Post.objects.order_by('-rating')[:5]
    content['categories'] = Category.objects.all().order_by('name')
    return render_to_response('home.html', content, context)

def register(request):
    context = RequestContext(request)
    content = {'message': ''}
    user_form = UserForm()
    profile_form = UserProfileForm()
    status = str(request.GET.get('from',''))

    # when user is logged in, disable register page
    if request.user.username != '':
        return HttpResponseRedirect('/')
    if request.method == 'POST':

        # cancel button functionality
        if 'cancel' in request.POST:
            if not status:
                return HttpResponseRedirect('/')
            else:
                return HttpResponseRedirect('/'+status)

        # grab data from forms
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        # check for validity then register this user
        if user_form.is_valid() and profile_form.is_valid() and request.POST['username'] != 'anonymous':
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            # this step is required for user's profile
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
            profile.save()

            # auto-login after register
            new_username = request.POST['username']
            new_password = request.POST['password']
            new_user = authenticate(username=new_username, password=new_password)
            login(request, new_user)
            return HttpResponseRedirect('/')
        content['message'] = 'Registration Voided' # show register failed message
    content['form'] = {'user': user_form, 'profile': profile_form}
    return render_to_response('register.html', content, context)

def user_login(request):
    context = RequestContext(request)
    content = {'message': ''}

    # if logged in, disable view
    if request.user.username != '':
        return HttpResponseRedirect('/')
    if request.method == 'POST':

        # when user clicks register, go to register page
        if 'register' in request.POST:
            return HttpResponseRedirect('/join?from=login')

        # heart of login process
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        # when authentication failed, show message
        if not user:
            content['message'] = 'Either username or password is incorrect'
            return render_to_response('login.html', content, context)

        # when user exists, login with inputted credentials
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect('/')
        content['message'] = 'Your account is currently disabled'
    return render_to_response('login.html', content, context)

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login')

def add_food(request):
    context = RequestContext(request)
    content = {'message': ''}
    form = PostForm()
    content['form'] = form
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    if request.method == 'POST':
        form = PostForm(data=request.POST)
        if form.is_valid():
            food = form.save(commit=False)
            food.author = request.user.username
            food.save()
        return HttpResponseRedirect('/food?id=%s&action=added' % str(food.id))
    return render_to_response('add_food.html', content, context)

def view_food(request):
    context = RequestContext(request)
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    try:
        value = int(request.GET.get('id','0'))
        food = Post.objects.get(id=value)
    except:
        raise Http404
    if request.method == 'POST':
        form = CommentForm(data=request.POST)
        comment = form.save(commit=False)
        comment.author = request.user
        comment.post = food
        comment.save()
        return HttpResponseRedirect('/food?id='+str(value))
    content = {'food': food, 'id': value, 'message': ''}
    action = str(request.GET.get('action','none'))
    profile = UserProfile.objects.get(user=request.user)
    comments = []
    for comment in Comment.objects.all().order_by('updated'):
        if comment.post.id == value:
            comments.append(comment)
    content['comments'] = comments
    if action == 'remove':
        if food.author == request.user or request.user.is_staff:
            food.delete()
            return HttpResponseRedirect('/?action=removed')
    elif action == 'added':
        content['message'] = 'Added Successfully'
    elif action == 'rate':
        if food in profile.rated.all():
            food.rating -= 1
            profile.rated.remove(food)
        else:
            food.rating += 1
            profile.rated.add(value)
        food.save()
        return HttpResponseRedirect('/food?id='+str(value))
    if food in profile.rated.all():
        content['like'] = 'Unlike'
    else:
        content['like'] = 'Like'
    content['comment_box'] = CommentForm()
    return render_to_response('view_food.html', content, context)

def edit_food(request):
    context = RequestContext(request)
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    try:
        value = int(request.GET.get('id','0'))
        food = Post.objects.get(id=value)
    except:
        raise Http404
    if request.method == 'POST':
        food = Post.objects.get(id=value)
        form = PostForm(data=request.POST, instance=food)
        form.save()
        return HttpResponseRedirect('/food?id='+str(value))
    form = PostForm(instance=food)
    content = {'form': form}
    return render_to_response('edit_food.html', content, context)

def search(request):
    errors = []
    if 'term' in request.GET:
        term = request.GET['term']
        if not term:
            errors.append('Enter a search term.')
        else:
            foods = Post.objects.filter(title__istartswith=term)
            results = [ x.title for x in foods ]
            return HttpResponse(json.dumps(results), content_type="application/json")
    return HttpResponse(json.dumps(errors), content_type="application/json")

def result(request):
    value = 0
    if 'term' in request.GET:
        foods = Post.objects.filter(title=request.GET['term'].strip())
        if foods:
            value = foods[0].id
        else:
            return HttpResponse('') # TODO handle this
    return HttpResponseRedirect('/food?id='+str(value))

def view_profile(request):
    context = RequestContext(request)
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    username = request.GET.get('username','')
    try:
        user = User.objects.get(username=username)
        profile = UserProfile.objects.get(user=user)
    except:
        raise Http404
    if 'action' in request.GET:
        if request.GET.get('action') == 'eliminiate':
            for food in profile.rated.all():
                food.rating -= 1
                food.save()
            for food in Post.objects.all():
                if food.author == username:
                    food.author = 'anonymous'
                    food.save()
            profile.delete()
            user.delete()
            request.user = ''
            return HttpResponseRedirect('/')
    visitor = UserProfile.objects.get(user=request.user)
    common = []
    for r in profile.rated.all():
        if r in visitor.rated.all():
            common.append(r)
    diff = len(profile.rated.all()) - len(common)
    content = {'profile': profile, 'common': common, 'diff': diff}
    return render_to_response('profile.html', content, context)

def edit_profile(request):
    context = RequestContext(request)
    content = {'message':''}
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    username = request.GET.get('username','')
    try:
        user = User.objects.get(username=username)
        profile = UserProfile.objects.get(user=user)
    except:
        user = request.user
        profile = UserProfile.objects.get(user=request.user)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            if not username:
                return HttpResponseRedirect('/profile')
            else:
                return HttpResponseRedirect('/profile?username='+username)
        user_form = UserForm(data=request.POST, instance=user)
        profile_form = UserProfileForm(data=request.POST, instance=profile)
        user = user_form.save()
        user.set_password(user.password)
        user.save()

        # this step is required for user's profile
        profile = profile_form.save(commit=False)
        profile.user = user
        if 'picture' in request.FILES:
            profile.picture = request.FILES['picture']
        profile.save()
        return HttpResponseRedirect('/')
    user_form = UserForm(instance=user)
    profile_form = UserProfileForm(instance=profile)
    content['form'] = {'user': user_form, 'profile': profile_form}
    return render_to_response('manage.html', content, context)

def comments(request):
    context = RequestContext(request)
    content = {'message':''}
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    try:
        value = int(request.GET.get('removed','0'))
        comment = Comment.objects.get(id=value)
    except:
        raise Http404
    food_val = comment.post.id
    if comment.author == request.user or request.user.is_staff:
        comment.delete()
    return HttpResponseRedirect('/food?id='+str(food_val))

def bug_report(request):
    context = RequestContext(request)
    content = {'message':''}
    user = request.user.username
    if not user:
        return HttpResponseRedirect('/login')
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect('/')
        report_form = ReportForm(data=request.POST)
        report = report_form.save(commit=False)
        report.author = request.user.username
        report.save()
        return HttpResponseRedirect('/report')
    content['report_form'] = ReportForm()
    return render_to_response('report.html', content, context)