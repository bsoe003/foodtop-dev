from django.db import models
from django.contrib.auth.models import User
import datetime

class Category(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class Post(models.Model):
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=200, unique=True)
    body = models.TextField()
    updated = models.DateTimeField(default=datetime.datetime.now)
    categories = models.ForeignKey(Category)
    rating = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Post)
    author = models.ForeignKey(User)
    content = models.TextField()
    updated = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.content

    def __str__(self):
        return self.content

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)
    rated = models.ManyToManyField(Post, blank=True)
    subscribed = models.ManyToManyField(Category, blank=True)

    def __unicode__(self):
        return self.user.username

    def __str__(self):
        return self.user.username

class Report(models.Model):
    author = models.CharField(max_length=200)
    subject = models.CharField(max_length=255)
    detail = models.TextField()
    date = models.DateTimeField(default=datetime.datetime.now)
    def __unicode__(self):
        return "%s by %s" % (self.subject, self.author)

    def __str__(self):
        return "%s by %s" % (self.subject, self.author)