$(document).ready(function(){
	var search_term_none = true;
	$(".empty").hide();
	$("#id_content").elastic();
	$("#term").autocomplete({
		source: "/search",
		response: function(event, ui) {
			search_term_none = (ui.content.length === 0);
		}
	});
	var time = 0;
	$("#login").click(function(){
		var form = document.forms["user_login"];
		for(var i = 0; i < form.elements.length; i++){
			var key = form.elements[i].name;
			if(key == 'login' || key == 'register')
				break;
			if(form[key].value == null || form[key].value == ''){
				$(".empty").html('You should not leave <b>'+key+'</b> blank').slideDown();
				return false;
			}
		}
	});

	$(".join").click(function(){
		if(!confirm("Is everything inputted correctly?"))
			return false;
		var form = document.forms["user_join"];
		var ignore = {'website': true, 'picture': true, 'register': true}
		return formConfirm(form, ignore);
	});

	$("#add").click(function(){
		if(!confirm("Is everything inputted correctly?"))
			return false;
		var form = document.forms["add_food"];
		var ignore = {'picture': true, 'add': true}
		return formConfirm(form, ignore);
	});

	$("#add_comment").click(function(){
		if(!confirm("Is everything inputted correctly?"))
			return false;
		var form = document.forms["add_comment"];
		var ignore = {'add': true};
		return formConfirm(form, ignore);
	});

	$("#send_report").click(function(){
		if(!confirm("Is everything inputted correctly?"))
			return false;
		var form = document.forms["bug_report"];
		var ignore = {'add': true};
		if(formConfirm(form, ignore)){
			alert("Thank you!");
		}
		else{
			return false;
		}
	});

	function formConfirm(form, ignore){
		var keep = '';
		for(var i = 0; i < form.elements.length; i++){
			var key = form.elements[i].name;
			if(key in ignore)
				break;
			if(form[key].value == null || form[key].value == ''){
				$(".empty").html('You should not leave <b>'+key+'</b> blank').slideDown();
				return false;
			}
			if(key == 'password'){
				keep = form[key].value;
			}
			if(key == 'confirm_password' && form[key].value != keep){
				$(".empty").html('Passwords do not match').slideDown();
				form['password'].value = '';
				form['confirm_password'].value = '';
				keep = '';
				return false;
			}
		}
		return true;	
	}

	$("#edit_profile").click(function(){
		var hash = window.location.href.slice(window.location.href.indexOf('?') + 1);
		window.location.href = "/manage?"+hash;
	});

	$("#remove_profile").click(function(){
		window.location.href += "&action=eliminiate";
	});

	$("#remove_food").click(function(){
		if(confirm("Are you sure you want to remove this?"))
			window.location.href = window.location.href + "&action=remove";
	});

	$("#edit_food").click(function(){
		var hash = window.location.href.slice(window.location.href.indexOf('?') + 1);
		window.location.href = "/edit?"+hash;
	});

	$(".sidebar").mouseover(function(){
		$(this).animate({ width: "175px"}, "fast");
		clearTimeout(time);
		time = setTimeout(function(){
			$(".select_name").show();
		}, 175);
		$(this).dequeue();
	});

	$(".sidebar").mouseleave(function(){
		$(this).animate({ width: "60px"}, "fast");
		clearTimeout(time);
		$(".select_name").hide();
		$(this).dequeue();
	});

	$("#search_toggle").click(function(){
		$(".searchbar").slideToggle(function(){
			$("#term").focus();
		});
	});

	document.getElementById('term').onkeypress = function(e){
		if (!e) e = window.event;
		var keyCode = e.keyCode || e.which;
		if (keyCode == '13'){
			var term = document.getElementById('term').value;
			if (term == null || term == ''){
				$.getJSON("/search?term="+term.toString(), function(data){
					alert(data);
				});
				return false;
			}
			if (search_term_none){
				alert("Result(s) not found!");
				return false;
			}
    	}
  	}
});