from foods.models import UserProfile, Post, Category, Comment, Report
from django.contrib.auth.models import User
from django import forms

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('website', 'picture')

class PostForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ('title', 'body', 'categories')

class CommentForm(forms.ModelForm):
    class Meta(object):
        model = Comment
        fields = ('content',)

class ReportForm(forms.ModelForm):
    class Meta(object):
        model = Report
        fields = ('subject', 'detail')