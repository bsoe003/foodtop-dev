from django.conf.urls import patterns, include, url
from django.contrib import admin
from foods import views

urlpatterns = patterns('',
	url(r'^$', views.home, name='home'),
    url(r'^login$', views.user_login, name='login'),
    url(r'^join$', views.register, name='register'),
    url(r'^profile', views.view_profile, name='view_profile'),
    url(r'^manage', views.edit_profile, name='edit_profile'),
    url(r'^add$', views.add_food, name='add_food'),
    url(r'^edit$', views.edit_food, name='edit_food'),
    url(r'^food$', views.view_food, name='view_food'),
    url(r'^comments$', views.comments, name='comments'),
    url(r'^search$', views.search, name='search'),
    url(r'^result', views.result, name='search_result'),
    url(r'^report', views.bug_report, name='bug_report'),
    url(r'^admin/', include(admin.site.urls)),
)
